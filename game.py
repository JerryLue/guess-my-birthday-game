from random import randint

name = input("Hi! What is your name?",)
num_guesses = 1

for num_guesses in range (1,6):
    month = randint(1,12)
    year = randint(1900,2023)
    guess_check = input("Hi "+name +" were you born in "+str(month)+"/"+str(year)+"?"+"(yes/no)",).lower
    if guess_check == "yes":
        print("I knew it!")
        break
    elif num_guesses == 5:
        print("I have other things to do. Good bye.")
        break
    else:
        print("Drat! Lemme try again!")
        num_guesses = num_guesses+1
